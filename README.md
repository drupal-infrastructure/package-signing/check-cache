Mount:
* `/data` webroot to monitor.

Environment variables:
* `SERVICE_URL` include trailing slash, like `https://example.com/`
* `FASTLY_TOKEN` Fastly account token for `PURGE` requests
