FROM registry.access.redhat.com/ubi9/ubi-minimal:latest@sha256:14f14e03d68f7fd5f2b18a13478b6b127c341b346c86b6e0b886ed2b7573b8e0

RUN microdnf install -y findutils

COPY app /app

USER 1001

ENTRYPOINT ["/app/check-cache.sh"]
